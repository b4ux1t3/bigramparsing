# Bigram Parsing

Setup is pretty straightforward. As long as you have .NET Core 3.1+ installed, all you have to do is

```
    git clone https://gitlab.com/b4ux1t3/bigramparsing
    cd bigramparsing
    dotnet run
```
This should open a browser to your localhost, over port 5001. If it does not, simply navigate to:

```
    https://locahost:5001
```

in your browser.
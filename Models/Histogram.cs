using System.Collections.Generic;
using System;
using System.Linq;

namespace BigramParsing.Models{
    public class Histogram{
        public int Width {get; protected set;}
        public int Height {get; protected set;}
        public int Scale = 50; // Number of pixels for each bar
        public int Padding = 5; // Space between our bars
        public Dictionary<string, string> Data;
        public Histogram(Dictionary<Tuple<string, string>, int> data){
            Width = (Scale * data.Count) + (Padding * (data.Count + 1));
            Height = data.Values.Max() * 10 + Padding * 2;
            Data = new Dictionary<string, string>();
            foreach (var item in data)
            {
                Data.Add($"{item.Key.Item1}, {item.Key.Item2}: {item.Value}", $"{item.Value * Scale}px");
            }
        }
    }
}